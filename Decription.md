## Work1

<details>
<summary>설명</summary>

![](./_DescriptionImage/Work1_0.png)

* 맵은 기본 Cube를 X축으로 200, Y축으로 200, Z축으로 1000 스케일 하여 표현했다. Cube의 각 면에 Rigidbody를 적용하여 플레이어, 적과 적 보스가 큐브 내부를 벗어나지 못하도록 하였다. Rigidbody는 플레이어, 적과 적 보스에도 적용되어 있으므로 공간에 부딪치면 반사 벡터의 방향으로 이동한다.
* 큐브맵 텍스쳐를 다운로드 받아 Texture Shape을 Cube로 설정한 후 맵에 적용하여 배경을 표현하였다. 플레이어가 맵의 벽과 충돌하는 동안 색이 파랑으로 바뀐다.
* 플레이어는 Z=-450에 위치하고 적 보스는 Z=450에 위치한다. 적은 육면체 모양으로 Blue, Green, Pink와 Red가 있다.
* 플레이어는 W, S키로 전, 후로 이동할 수 있으며 A, D 키로 좌, 우, Q, E 키로 상, 하로 움직일 수 있다. Input.GetKey()를 사용하여 키를 입력 받아 연속으로 움직일 수 있게 하였다. Input.GetAxix()를 사용하여 마우스의 X축 위치에 따라 플레이어는 Y축을 기준으로 회전하고, 마우스의 Y축 위치에 따라 플레이어는 X축을 기준으로 회전해 마우스 위치의 반대방향으로 플레이어가 전진하게 만들었다. 또한, 마우스 휠 스크롤에 따라 캐릭터는 Z축을 기준으로 회전한다.
* 이동 중에는 위 그림과 같은 사각형이 나타난다. GameObject.SetActive()를 사용하여 이동 중에만 나타나도록 하였다. 
* 플레이어가 Enemy태그를 가진 적과 충돌하면 SceneManager.LoadScene()을 사용하여 게임을 다시 시작할 지 물어보는 Scene으로 전환한다. 아무 키나 입력되면 GameScene을 다시 시작한다.

![](_DescriptionImage/Work1_1.png)

* 플레이어는 W, S키로 전, 후로 이동할 수 있으며 A, D 키로 좌, 우, Q, E 키로 상, 하로 움직일 수 있다. Input.GetKey()를 사용하여 키를 입력 받아 연속으로 움직일 수 있게 하였다. Input.GetAxix()를 사용하여 마우스의 X축 위치에 따라 플레이어는 Y축을 기준으로 회전하고, 마우스의 Y축 위치에 따라 플레이어는 X축을 기준으로 회전해 마우스 위치의 반대방향으로 플레이어가 전진하게 만들었다. 또한, 마우스 휠 스크롤에 따라 캐릭터는 Z축을 기준으로 회전한다.
* 이동 중에는 위 그림과 같은 사각형이 나타난다. GameObject.SetActive()를 사용하여 이동 중에만 나타나도록 하였다. 
* 플레이어가 Enemy태그를 가진 적과 충돌하면 SceneManager.LoadScene()을 사용하여 게임을 다시 시작할 지 물어보는 Scene으로 전환한다. 아무 키나 입력되면 GameScene을 다시 시작한다.

![](_DescriptionImage/Work1_2.png)

* 플레이어는 20개의 총알을 기본으로 가지며, 왼쪽 Ctrl키를 사용하여 발사할 수 있다. 이 때, 총알은 발사되는 순간의 플레이어의 로컬 forward를 방향벡터로 가지고 이동하며, 자신의 Z축에 대해 회전하면서 이동한다. 총알이 발사되면 ammo는 1 감소하고, 초당 100미터 이동하며 2초 후 사라진다. 만약 적을 맞췄다면 ammo는 1 증가한다.
* Z키를 누르면 Physics.OverlapSphere()을 이용하여 플레이어의 위치로부터 30미터 이내에 있는 “Enemy”태그를 가진 GameObject를 얻고 Enemy들의 Hit()함수를 SendMessage()로 호출한다. 이를 통해 적의 ParticleSystem을 Play()한다. 이 때, 플레이어의 SpecialAmmo는 1 감소한다.
* 마우스 왼쪽 버튼을 클릭한 경우 Raycast를 현재 마우스 위치에서 실시해 충돌하는 적이 있는지 검사한다. 만약 있다면, UI에 free 대신 locked라고 표시된다. 이 때, 왼쪽 Ctrl을 누르면 이 총알은 선택된 적을 향해 이동이며 Quaternion.LookRotation()을 사용해 목표를 바라보며, 초당 100미터 이동하고 명중할 때 까지 사라지지 않는다. 또한, 명중 여부와 상관없이 발사하는 순간 자동 조준은 해제된다.

![](_DescriptionImage/Work1_3.png)

![](_DescriptionImage/Work1_4.png)

* 적은 플레이어의 forward 방향 100미터에서 생성되며, Blue, Green, Pink 색상의 적은 2초마다 셋 중 하나의 색상을 무작위로 가지고 생성되며 Red 색상의 적은 10초마다 생성된다. Rigidbody의 AddForce()와 AddTorque()를 사용해 무작위로 이동하고 회전하며, 생성된 후 2초가 지나면 FixedUpdate()가 한 번 호출될 때 마다 플레이어를 향한 벡터의 0.2만큼 AddForce()로 힘이 가해진다.
* 만약 적이 총알에 맞는다면 100 조각 이상의 큐브가 생기는 ParticleSystem을 Play()하고, Red 색상의 적이 총알과 충돌했다면 플레이어의 SpecialAmmo가 1 증가한다.
* Tag가 “Enemy”인 적을 모두 얻어 Vector3.Angle()함수와 플레이어의 forward벡터, 적이 향하는 방향을 구해 적이 플레이어 전진 방향의 뒤에 있는지 판단하고, 만약 있다면 분홍색으로 색상이 바뀐다.

![](_DescriptionImage/Work1_5.png)

* 적 보스는 -Z방향을 바라보고 있으며, 생성될 때 -Z방향으로 500, 다른 방향으로는 무작위 값의 힘을 가해진다. 자신의 forward를 향해 총알을 2초마다 발사하며, 4초간 존재한다. 이외에는 플레이어의 기본 총알 발사와 같다.

![](_DescriptionImage/Work1_6.png)

* UICanvas에 Text를 넣어 ammo, 자동조준 상태, specialAmmo, score가 변화할 때 마다 텍스트를 업데이트하며 Z키를 눌러서 죽인 적은 10점, 자동조준으로 죽인 적은 20점, 그냥 발사해 죽인 적은 50점을 얻는다.

![](_DescriptionImage/Work1_7.png)

* 플레이어가 적 보스를 총알로 맞춘다면 위 Scene으로 전환되고 아무 키나 눌러서 게임을 종료할 수 있다.

![](_DescriptionImage/Work1_8.png)
* 플레이어가 적과 충돌하거나 적 보스의 총알에 맞으면 위 Scene으로 전환되고 ESC를 누르면 게임을 종료, 이외의 아무 키나 누르면 게임을 GameScene을 다시 불러온다.

</details>

---

## Work2

<details>
<summary>설명</summary>

![](_DescriptionImage/Work2_0.png)

* 갤러그를 모작했으며, 좌우 방향키로 이동, 스페이스로 총알을 발사할 수 있다.
* 배경은 다른 속도를 가진 두 개의 유니티 파티클 시스템으로 표현했다.
* 스테이지가 시작되면 적은 지정된 위치를 향해 이동하고, [2, 4]초마다 무작위로 총알을 발사한다.
* 적 한 명당 25점의 점수를 얻으며, 적을 모두 없애면 다음 스테이지로 넘어간다.
* 플레이어는 세 개의 목숨을 가지며, 목숨이 모두 사라질 때까지 스테이지가 진행된다.
* Coroutine을 통해 게임 장면을 전환하고 실행한다.

![](_DescriptionImage/Work2_1.png)

* 스테이지가 시작되면 적은 미리 설정된 EnemyPos을 향해 이동하며, 총 18개가 있다. EnemyPos는 EnemyForm의 자식이며, EnemyForm은 좌우로 이동한다. 무작위의 적이 선택되어 [2, 4]초 후 총알을 발사한다.

![](_DescriptionImage/Work2_2.png)

* 적은 기본적으로 Idle상태이며, Die파라미터가 true가 되면 애니매이터가 Explode 상태로 전이되어 emeny_all_explode모션을 실행한다.

![](_DescriptionImage/Work2_3.png)

* 플레이어는 좌우 방향키로 이동할 수 있으며, 스페이스를 누르면 총알을 발사한다. 이 때, 총알 발사에는 0.5초의 딜레이가 있다. 적을 제거했다면 점수가 25점 증가한다.
* Collider로 충돌체크를 해 맵 바깥으로 나가지 못 하게 하는 대신 Mathf.Clmap()를 사용해 일정 좌표 바깥으로 이동할 수 없게 했다. 다만 총알은 맵 바깥에 Bound를 만들어 충돌하면 사라지게 했다.
* 애니매이터 설정은 적과 같으며, 만약 적의 총알에 맞는다면 폭발 애니매이션이 재생된다. 이때, 목숨이 아직 남아있었다면 GameController 스크립트에 의해 다시 생성된다.
* 목숨을 모두 소모했다면, GameScene코루틴이 종료되고 다시 EntryScene부터 시작된다.

</details>

---

## Work3

<details>
<summary>설명</summary>

![](_DescriptionImage/Work3_0.png)

* 크레이지 아케이드(BnB)를 모작했다. 게임은 두 플레이어가 진행하며 한 쪽이 죽을 때까지 계속된다.
* 플레이어 1은 w, a, s, d로 전, 후, 좌, 우 이동 및 스페이스로 물풍선을 놓을 수 있으며 플레이어2는 화살표 키로 전, 후, 좌, 우 이동 및 엔터키로 물풍선을 놓을 수 있다.
* 어느 한 플레이어가 죽거나 둘 다 죽으면 승리 텍스트가 뜨며, 아무 키나 눌러서 재시작 할 수 있다.

![](_DescriptionImage/Work3_1.png)

* 먼저 리소스를 구해야 했는데 구글링을 해도 나오지 않아서 BnB플래시 데모를 다운받아 디컴파일해 이미지를 가져왔다. 그 후 포토샵으로 적절하게 수정해 유니티에서 스프라이트를 불러왔다.

![](_DescriptionImage/Work3_2.png)

* 특히 물풍선이 골치 아팠는데, 물풍선이 놓인 부분과 물풍선이 뻗어 나가는 중간 부분, 마지막 부분이 모두 달라 처리하기 약간 어려웠다.
* 플레이어는 먼저 프리팹을 만들고 [1, 2] 범위의 playerNumber 변수를 주어 이 값에 따라 움직이는 키를 다르게 할당했다. 그리고 적절한 위치에 배치한 후, 어느 플레이어인지 알아보게 하기 위해 텍스트가 따라다니게 했다. 블록에 충돌하더라도 부드럽게 움직이게 하기 위해 CircleCollider를 사용했다.

![](_DescriptionImage/Work3_3.png)

* 플레이어는 최대 물풍선의 개수, 속도, 물풍선의 파워를 표현하는 변수를 가지고 있으며, 각각에 대응하는 아이템이 등장한다.

![](_DescriptionImage/Work3_4.png)

![](_DescriptionImage/Work3_5.png)

![](_DescriptionImage/Work3_6.png)

* 먼저 바닥 타일을 맵에 일일이 놓기에는 시간이 너무 오래 걸리므로 Instatiate()를 사용해 생성했다. 그 후 게임이 리셋될 때 마다 스테이지 프리팹을 Destroy()후 다시 생성한다.
* 풍선은 코루틴을 사용하여 구현했다. 전, 후, 좌, 우의 방향으로 BoomCheck 레이어에 Raycast2D를 실시해 막히지 않았으면 플레이어의 파워까지 계속 생성, 아니라면 멈춘다. 단, Dtorable태그를 가진 블록에 대해서는 막혔어도 한 번 무시한다.

![](_DescriptionImage/Work3_7.png)

![](_DescriptionImage/Work3_8.png)

* 아이템은 최대 물풍선의 개수, 속도, 물풍선의 파워 증가가 있으며, Box를 파괴하면 3/10 확률로 등장한다.

</details>

---

## Work4

**PlatformerMicrogame 분석 및 수정**

<details>
<summary>설명</summary>

![](_DescriptionImage/Work4_0.png)

* 먼저 백그라운드 이미지를 모두 4배의 크기에 맞게 위치를 조절하고 새로 넣었다. 그리고 BackgroundParallex 스크립트의 ParallexScale, ParallexReductionFactor의 값을 각각 0.4, 0.2로 바꿔 넓어진 맵에서도 괜찮게 보이도록 하였다.
* 그리고 KillTriger도 넓어진 맵에 맞게 길이를 늘렸다. 플랫폼은 적당히 올라갈 수 있을 정도로 배치했다. 중간의 탑은 BoxCollider2D를 수정해 탑의 문 모양쪽으로는 지나갈 수 있게 변경했다.

![](_DescriptionImage/Work4_1.png)

![](_DescriptionImage/Work4_2.png)

* 적 스포너가 고정된 위치에 있어서 넓어진 맵에 알맞지 않았다. 따라서 x축 [-100, 100]범위에서 플레이어를 따라다니도록 했다. 단, 게임 시작 시 플레이어와 스포너의 거리를 계산해 오프셋을 주어 스포너끼리의 간격은 유지한다.
* 픽업은 고정된 범위 대신 플레이어의 현재 위치의 x좌표 + [-20, 20]범위에서 무작위로 생성되게 했다. 그리고 한 개만 가질 수 있는 대신 처음에 DeliverPickup 코루틴을 3번 호출해 최대 3개의 폭탄을 가질 수 있게 했다. 따라서 UI도 수정했다.

![](_DescriptionImage/Work4_3.png)

</details>

---

## Mid

**대학교 강의 중간과제로 제출**

<details>
<summary>설명</summary>

![](_DescriptionImage/Mid_0.png)

* 제안서대로 ITO2에서 몇 가지를 변경했다. 플레이어는 흰색 큐브, 맵은 스테이지 대신 미리 만들어 둔 15 종류의 chunk가 무작위로 10개 생성된다. 각 chunk는 가로 3unit, 세로 5unit, 높이 1unit이다.
* 초록 사과 대신 파란 구를 사용했으며 충돌 시 오른쪽 이동 후 전진, 빨간 사과 대신 빨간 구를 사용했으며 충돌 시 전진한다.
* 왼쪽 상단의 버튼을 이용하여 명령 대기열에 넣을 수 있으며, 각 버튼의 의미는 다음과 같다.

* ![](_DescriptionImage/Mid_1.png) 한 칸 전진

* ![](_DescriptionImage/Mid_2.png) 오른쪽으로 한 칸 이동

* ![](_DescriptionImage/Mid_3.png) 왼쪽으로 한 칸 이동

* 20초안에 명령을 모두 입력해야 하며, 명령을 실행 후 1chunk를 넘어가지 못하면 게임이 끝난다. 스페이스를 누르면 실행시킬 수 있다. 만약 20초안에 명령을 입력하지 않았다면, 플레이어는 한 칸 전진한다.
* 장애물에 부딪히거나 맵 바깥으로 나가면 게임에 패배하며, 맵을 모두 지났다면 다음 스테이지로, 마지막 스테이지라면 승리 화면으로 넘어간다.

| [Dead] | [Dead] | [Dead] | [Dead] | [Dead]     | ... | [End] |
|--------|--------|--------|--------|------------|-----|-------|
| SL     | [None] | [None] | [None] | [Clear] EL | ... | [End] |
| SM     | [None] | [None] | [None] | [Clear] EM | ... | [End] |
| SR     | [None] | [None] | [None] | [Clear] ER | ... | [End] |
| [Dead] | [Dead] | [Dead] | [Dead] | [Dead]     | ... | [End] |

* SL, SM, SR, EL, EM, ER: bool
    * 각 변수는 이 칸이 비어 있는지 표현한다. 즉, 지나갈 수 있는 곳인지 표현한다.
    * 맵 생성은 무작위로 미리 만든 chunk를 하나 고른다. 위 변수를 이용해 길이 연결되는지 판별한 후, 지나갈 수 있다면 배열에 넣는다. 즉, (curr.SL && next.EL) || (curr.SM && next.EM) || (curr.SR && next.ER) 이 식이 참이라면 다음 chunk로 선택한다.
    * 처음에는 비트연산으로 하려 했으나 inspector에서 이진수나 16진수를 입력할 수 없어 불편해 bool을 여러 개 넣었다.
    * 각 chunk는 5unit의 길이를 가지므로 배열을 순회해 z를 5씩 증가시키며 Instantiate()한다.
    * 다만 맨 처음 chunk는 ChunkA로 고정한다.

* Dead, None, Clear, End: Unity Tag
    * Dead 태그를 가진 collider와 충돌이 발생하면 게임 실패로 생각한다.
    * None 태그를 가진 collider에 8초 이상 머무르면 게임 실패로 생각한다.
    * Clear 태그를 가진 collider와 충돌이 발생하면 그 chunk를 클리어했다고 생각하고, 타이머와 입력한 명령을 초기화한 후 다음 chunk로 한 칸 전진한다.
    * End 태그를 가진 collider와 충돌이 발생하면 스테이지를 클리어 한 것으로 생각하고, 다음 장면으로 넘어간다.

![](_DescriptionImage/Mid_4.png)

![](_DescriptionImage/Mid_5.png)

* 전진은 1, 왼쪽 이동은 2, 오른쪽 이동은 3으로 정했다. 좌측 상단의 이미지를 눌러 최대 6개까지 명령을 추가 가능하다.
* 스페이스나 시간제한이 끝난 순간 Invoke()를 사용하여 순서대로 이동을 실행한다.
* 단, 아이템에 충돌했을 경우 CancelInvoke()를 호출해 앞으로의 모든 이동을 취소한 후, 아이템에 따른 이동을 먼저 실행한다. 이 때, 나머지 이동이 다시 실행되어야 하므로 이동이 실행될 때 마다 idx를 증가시키고, idx위치부터 명령을 다시 Invoke()한다.
* 이동이 모두 끝난 후 End에 충돌했다면 명령 배열을 비우고, UI와 타이머를 초기화한다.

![](_DescriptionImage/Mid_6.jpg)

</details>
