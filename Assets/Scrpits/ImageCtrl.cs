﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class ImageCtrl
    : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1.5f, 1.5f, 1f);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (gameObject.tag)
        {
            case "Scene1":
                SceneManager.LoadScene("01_GameScene");
                break;
            case "SceneMid":
                SceneManager.LoadScene("Mid_EntryScene");
                break;
            case "Scene2":
                SceneManager.LoadScene("02_GameScene");
                break;
            case "Scene3":
                SceneManager.LoadScene("03_GameScene");
                break;
            case "Scene4":
                SceneManager.LoadScene("04_Level");
                break;
        }
    }
}
