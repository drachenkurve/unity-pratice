﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkGen : MonoBehaviour {
	public GameObject ChunkA;
	public GameObject ChunkB;
	public GameObject ChunkC;
	public GameObject ChunkD;
	public GameObject ChunkE;
	public GameObject ChunkF;
	public GameObject ChunkG;
	public GameObject ChunkH;
	public GameObject ChunkI;
	public GameObject ChunkJ;
	public GameObject ChunkK;
	public GameObject ChunkL;
	public GameObject ChunkM;
	public GameObject ChunkN;
	public GameObject ChunkO;
	GameObject[] chunks = new GameObject[10];
	// Use this for initialization
	void Start () {
		chunks[0] = ChunkA;

		for (int i = 1; i < 10; ++i) {
			GameObject c;
			while (true) {
				c = Pick();
				if (
					(c.GetComponent<Chunk>().SL && chunks[i - 1].GetComponent<Chunk>().EL) ||
					(c.GetComponent<Chunk>().SM && chunks[i - 1].GetComponent<Chunk>().EM) ||
					(c.GetComponent<Chunk>().SR && chunks[i - 1].GetComponent<Chunk>().ER)
				) {
					chunks[i] = c;
					break;
				}
			}
		}

		for (int i = 0; i < 10; ++i) {
			Instantiate(chunks[i], new Vector3(0.0f, 0.0f, 5.0f * i), Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	GameObject Pick() {
		switch(Random.Range(1, 15)) {
			case 1:
				return ChunkB;
			case 2:
				return ChunkC;
			case 3:
				return ChunkD;
			case 4:
				return ChunkE;
			case 5:
				return ChunkF;
			case 6:
				return ChunkG;
			case 7:
				return ChunkH;
			case 8:
				return ChunkI;
			case 9:
				return ChunkJ;
			case 10:
				return ChunkK;
			case 11:
				return ChunkL;
			case 12:
				return ChunkM;
			case 13:
				return ChunkN;
			case 14:
				return ChunkO;
		}
		return ChunkA;
	}
}
