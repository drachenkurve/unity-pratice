﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCtrl_Mid : MonoBehaviour {
	public GameObject UiUp;
	public GameObject UiLeft;
	public GameObject UiRight;
	public GameObject UiTimer;
	public GameObject UiContainer;
	public GameObject player;
	public ContainerCtrl containerCtrl;
	public bool executing = false;
	float acc;
	public int timerValue;
	int timer;
	// Use this for initialization
	void Start () {
		timer = timerValue;
		UiTimer.GetComponent<Text>().text = "" + timer;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > 0 && !executing) {
			acc += Time.deltaTime;
			if (acc >= 1.0) {
				--timer;
				UiTimer.GetComponent<Text>().text = "" + timer;
				acc = 0;
			}
		} else if (timer <= 0 && !executing) {
			if (!UiContainer.GetComponent<ContainerCtrl>().Execute()) {
				player.transform.position += Vector3.forward;
				Reset();
			}
			executing = true;
		}
		if (Input.GetKey(KeyCode.Space) && !executing) {
			UiContainer.GetComponent<ContainerCtrl>().Execute();
			executing = true;
		}
	}

	public void Reset() {
		timer = timerValue;
		UiTimer.GetComponent<Text>().text = "" + timer;
		executing = false;
	}
}
