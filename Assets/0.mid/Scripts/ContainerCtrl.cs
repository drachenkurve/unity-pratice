﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerCtrl : MonoBehaviour {
	public GameObject UiUp;
	public GameObject UiLeft;
	public GameObject UiRight;
	public int[] actions = new int[6];
	public GameObject[] actionImgs = new GameObject[6];
	public PlayerCtrl playerCtrl;
	int idx;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void AddUp() {
		if (idx < 6) {
			actions[idx] = 1;
			actionImgs[idx] = Instantiate(UiUp);
			actionImgs[idx].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
			actionImgs[idx].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			actionImgs[idx].transform.position = new Vector3(40.0f * idx + 25.0f, 40.0f, 0.0f);
			++idx;
		}
	}

	public void AddLeft() {
		if (idx < 6) {
			actions[idx] = 2;
			actionImgs[idx] = Instantiate(UiLeft);
			actionImgs[idx].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
			actionImgs[idx].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			actionImgs[idx].transform.position = new Vector3(40.0f * idx + 25.0f, 40.0f, 0.0f);
			++idx;
		}
	}

	public void AddRight() {
		if (idx < 6) {
			actions[idx] = 3;
			actionImgs[idx] = Instantiate(UiRight);
			actionImgs[idx].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
			actionImgs[idx].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			actionImgs[idx].transform.position = new Vector3(40.0f * idx + 25.0f, 40.0f, 0.0f);
			++idx;
		}
	}

	public bool Execute() {
		if (actions[0] == 0) {
			return false;
		}
		for (int i = 0; i < 6; ++i) {
			switch(actions[i]) {
			case 1:
				playerCtrl.Invoke("MoveUp", 1.0f * i);
				break;
			case 2:
				playerCtrl.Invoke("MoveLeft", 1.0f * i);
				break;
			case 3:
				playerCtrl.Invoke("MoveRight", 1.0f * i);
				break;
			}
		}
		return true;
	}

	public void ReExecute(int prevIdx, int offset) {
		for (int i = prevIdx; i < 6; ++i) {
			switch(actions[i]) {
			case 1:
				playerCtrl.Invoke("MoveUp", 1.0f * (i - prevIdx + offset));
				break;
			case 2:
				playerCtrl.Invoke("MoveLeft", 1.0f * (i - prevIdx + offset));
				break;
			case 3:
				playerCtrl.Invoke("MoveRight", 1.0f * (i - prevIdx + offset));
				break;
			}
		}

	}
	public void Flush() {
		for (int i = 0; i < 6; ++i) {
			actions[i] = 0;
			Destroy(actionImgs[i]);
			actionImgs[i] = null;
		}
		idx = 0;
	}
}
