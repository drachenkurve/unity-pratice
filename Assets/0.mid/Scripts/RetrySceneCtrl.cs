﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetrySceneCtrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape)) {
			SceneManager.LoadScene("MainScene");
		}
		else if (Input.anyKey) {
			SceneManager.LoadScene("Mid_GameScene1");
		}
	}
}
