﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickAction : MonoBehaviour, IPointerClickHandler {
	public ContainerCtrl containerCtrl;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerClick(PointerEventData eventData) {
		if (name == "Up") {
			Debug.Log("UP");
			containerCtrl.AddUp();
		} else if (name == "Left") {
			Debug.Log("LEFT");
			containerCtrl.AddLeft();
		} else if (name == "Right") {
			Debug.Log("RIGHT");
			containerCtrl.AddRight();
		}
	}
}
