﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour {
	public GameObject player;
	Vector3 offset;
	float speed = 5.0f;
	// Use this for initialization
	void Start () {
		offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 dest = player.transform.position + offset;
		transform.position = Vector3.Lerp(transform.position, dest, speed * Time.deltaTime);
	}
}
