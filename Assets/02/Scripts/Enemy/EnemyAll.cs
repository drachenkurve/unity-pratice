﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAll : MonoBehaviour {

	public AudioClip enemy_death_sound;

	public bool shouldShootBack = true;
	public GameObject shootObject;

	private bool moving = false;

	void EnemyDetach () {
		moving = false;
		transform.SetParent (null);
		if (GetComponent<AudioSource> () != null && enemy_death_sound != null) {
			GetComponent<AudioSource> ().clip = enemy_death_sound;
			GetComponent<AudioSource> ().Play ();
		}
	}

	void EnemyDestroy () {
		GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().AddScore (25);
		Destroy (gameObject);
	}

	void Start () {
		transform.localPosition = new Vector3 (Random.Range (-5, 5), Random.Range (5, 7));

		moving = true;
	}

	void Update () {
		if (moving) {
			transform.localPosition = Vector3.Lerp (transform.localPosition, new Vector3 (), Time.deltaTime * 2);
			if (transform.localPosition == new Vector3 ()) {
				moving = false;
			}
		}
	}
}
