﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public string thisTag = "Player";

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag != "Bound" && coll.gameObject.tag != thisTag)
        {
            if (coll.gameObject.GetComponent<Animator>() != null)
            {
                coll.gameObject.GetComponent<Animator>().SetBool("Die", true);
            }
            else
            {
                Destroy(coll.gameObject);
            }

            if (gameObject.GetComponent<Animator>() != null)
            {
                gameObject.GetComponent<Animator>().SetBool("Die", true);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
