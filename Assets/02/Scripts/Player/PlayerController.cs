﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private Rigidbody2D rb;
    private Animator anim;
    private float nextShot;

    public bool canShoot = true;
    public float speed = 3;
    public Vector2 clamp = new Vector2(-8.0f, 8.0f);
    public float fireRate = 0.5f;
    public GameObject shotPrefab;

    private bool playing = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!anim.GetBool("Die"))
        {
            Vector2 vel = rb.velocity;
            vel.x = Input.GetAxis("Horizontal") * speed;
            rb.velocity = vel;

            Vector2 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, clamp.x, clamp.y);
            transform.position = pos;

            if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextShot && canShoot)
            {
                nextShot = Time.time + fireRate;
                Instantiate(shotPrefab, transform.position + new Vector3(0, 0.6f, 0), Quaternion.identity);
            }
        }
        else
        {
            playing = true;
        }
    }

    void Die()
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().NotiHit();
        Destroy(gameObject);
    }
}
