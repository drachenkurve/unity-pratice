﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	private static LevelManager Instance;

	void Awake () {
		if (Instance != null) {
			Destroy (gameObject);
			return;
		}
		GameObject.DontDestroyOnLoad (gameObject);
		Instance = this;
	}

	public void LoadLevel (string name) {
		SceneManager.LoadScene (name);
	}

	public void LoadNextLevel () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	public void QuitGame () {
		Application.Quit ();
	}
}
