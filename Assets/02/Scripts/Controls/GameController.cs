﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    enum State
    {
        Entry,
        InGame,
        GameOver
    };

    public Text startText;
    public Text stageText;
    public Text scoreText;
    public GameObject medal;
    public GameObject life;

    public Sprite lifeSprite;

    public GameObject playerPrefab;

    public int score;
    public int lives = 3;
    public int stage = 1;
    private State state = State.Entry;
    private List<GameObject> lifeImgs = new List<GameObject>();


    void Start()
    {
        startText.enabled = false;
        stageText.enabled = false;
        scoreText.enabled = false;

        StartCoroutine(EntryScene());
    }

    void Update()
    {
        if (state == State.Entry && Input.anyKeyDown)
        {
            StartCoroutine(GameScene());
        }

        UpdateGUI();
    }

    IEnumerator GameScene()
    {
        state = State.InGame;
        SpawnPlayer();
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().canShoot = false;
        yield return new WaitForSeconds(2);
        GameObject.FindGameObjectWithTag("EnemyFormation").GetComponent<EnemySpawner>().SpawnUntilFull();
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().canShoot = true;

    }

    void UpdateGUI()
    {
        scoreText.enabled = (state == State.InGame);
        scoreText.text = "score: " + score.ToString();
        if (state == State.InGame && lifeImgs.Count != lives)
        {
            foreach (GameObject obj in lifeImgs)
            {
                Destroy(obj);
            }
            lifeImgs.Clear();
            for (int i = 0; i < lives; i++)
            {
                GameObject obj = new GameObject("Life" + i.ToString());
                obj.AddComponent<Image>().sprite = lifeSprite;
                obj.transform.SetParent(life.transform);
                obj.transform.position = new Vector3((i * 30.0f) + 20.0f, 15.0f);
                obj.transform.localScale = new Vector3(0.2f * 0.9f, 0.2f);
                lifeImgs.Add(obj);
            }
        }
        else if (state == State.GameOver)
        {
            StartCoroutine(ResetGame());
        }
    }

    void SpawnPlayer()
    {
        Instantiate(playerPrefab, new Vector3(0, -4, 0), Quaternion.identity);
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().canShoot = true;
    }

    public void AddScore(int sc)
    {
        score += sc;
    }

    public void NotiHit()
    {
        StartCoroutine(WhenHit());
    }

    public void NotiAllDead()
    {
        StartCoroutine(WhenAllDead());
    }

    IEnumerator WhenHit()
    {
        --lives;
        yield return new WaitForSeconds(2);
        if (lives <= 0)
        {
            state = State.GameOver;
        }
        else
        {
            SpawnPlayer();
        }
    }

    IEnumerator WhenAllDead()
    {
        ++stage;
        stageText.text = "Stage " + stage.ToString();
        stageText.enabled = true;
        GameObject.FindGameObjectWithTag("EnemyFormation").GetComponent<EnemySpawner>().SpawnUntilFull();
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().canShoot = false;
        yield return new WaitForSeconds(2);
        stageText.enabled = false;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().canShoot = true;
    }

    IEnumerator EntryScene()
    {
        while (state == State.Entry)
        {
            startText.enabled = !startText.enabled;
            yield return new WaitForSeconds(1);
        }
        startText.enabled = false;
    }

    IEnumerator ResetGame()
    {
        yield return new WaitForSeconds(2);
        state = State.Entry;
        score = 0;
        StartCoroutine(EntryScene());
    }
}
