﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameCtrl_01 : MonoBehaviour {
	public GameObject player;
	public GameObject boss;
	public bool isVictory;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!player.GetComponent<PlayerCtrl_01>().isAlive) {
			SceneManager.LoadScene("01_RetryScene");
		}
		if (boss == null) {
			SceneManager.LoadScene("01_VictoryScene");
		}
	}
}
