﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBulletCtrl : MonoBehaviour {
	GameObject boss;
	GameObject player;
	Vector3 direction;
	// Use this for initialization
	void Start () {
		boss = GameObject.Find("Boss");
		player = GameObject.FindWithTag("Player");
		direction = boss.transform.forward;
		transform.rotation = boss.transform.rotation;
		Destroy(gameObject, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0.0f, 0.0f, 180.0f * Time.deltaTime);
		transform.position += direction * Time.deltaTime * 100.0f;
	}

	
	protected void OnCollisionEnter(Collision col) {
		if (col.collider.tag == "Player") {
			player.GetComponent<PlayerCtrl_01>().isAlive = false;
		}
	}
}
