﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFireCtrl : MonoBehaviour {
	public GameObject player;
	public GameObject bullet;
	float fireDelay = 2.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		fireDelay -= Time.deltaTime;
		if (fireDelay < 0.0f) {
			Destroy(Instantiate(bullet,
				transform.position,
				Quaternion.identity), 4.0f);
			fireDelay = 2.0f;
		}
	}
}
