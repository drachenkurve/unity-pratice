﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCtrl : MonoBehaviour {
	public GameObject player;
	public bool isAlive = true;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (!isAlive) {
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter(Collision col) {
		if (col.collider.tag == "Bullet") {
			isAlive = false;
		}
	}
}
