﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveCtrl : MonoBehaviour {
	public GameObject cubicEffect;
	float speed = 10.0f;
	bool hasCubicEffect;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		MoveCtrl();
	}

	void OnCollisionEnter(Collision col) {
		if (col.collider.tag == "World") {
			GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 1.6f);
		}
		if (col.collider.tag == "Enemy") {
			GetComponent<PlayerCtrl_01>().isAlive = false;
		}
	}

	void OnCollisionExit(Collision col) {
		if (col.collider.tag == "World") {
			GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
		}
	}

	void MoveCtrl() {
		if(Input.anyKey && GetComponent<PlayerCtrl_01>().isAlive) {
			if (Input.GetKey(KeyCode.W)) {
				hasCubicEffect = true;
				transform.position += transform.forward * speed * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.S)) {
				hasCubicEffect = true;
				transform.position -= transform.forward * speed * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.A)) {
				hasCubicEffect = true;
				transform.position -= transform.right * speed * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.D)) {
				hasCubicEffect = true;
				transform.position += transform.right * speed * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.Q)) {
				hasCubicEffect = true;
				transform.position += transform.up * speed * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.E)) {
				hasCubicEffect = true;
				transform.position -= transform.up * speed * Time.deltaTime;
			}
		} else {
			hasCubicEffect = false;
		}
		cubicEffect.SetActive(hasCubicEffect);
	}
}
