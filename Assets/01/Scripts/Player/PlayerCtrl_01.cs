﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl_01 : MonoBehaviour {
	public bool isAlive = true;
	public bool isDanger;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		isDanger = false;

		GameObject[] enemies;
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject enemy in enemies) {
			Vector3 dir = transform.position - enemy.transform.position;
			float angle = Vector3.Angle(transform.forward, dir);
			if (angle < 90.0f) {
				isDanger = true;
			}
		}

		if (isDanger) {
			GetComponent<Renderer>().material.color = new Color(1.0f, 0.4f, 0.7f);
		} else {
			GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);	
		}

		if (!isAlive) {
			GetComponent<MeshRenderer>().enabled = false;
		}
	}
}
