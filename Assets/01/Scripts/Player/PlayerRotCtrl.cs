﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotCtrl : MonoBehaviour {
	public bool isDanger;
	float mouseX, mouseY, mouseWh;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		RotCtrl();
	}

	void RotCtrl() {
		mouseX = Input.GetAxis("Mouse X");
		mouseY = Input.GetAxis("Mouse Y");
		mouseWh = Input.GetAxis("Mouse ScrollWheel");
		transform.Rotate(mouseY * 0.8f,
			mouseX * -0.8f,
			mouseWh * 10.0f);
	}
}
