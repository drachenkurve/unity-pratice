﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireCtrl : MonoBehaviour {
	public GameObject bullet;
	public GameObject lockedBullet;
	public int ammo = 20;
	public int specialAmmo = 10;
	public bool isLocked;
	public GameObject lockedTarget;
	float fireDelay;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<PlayerCtrl_01>().isAlive) {
			Lock();
			Fire();
			SpecialFire();
			UICtrl.instance.UpdateAmmo(ammo);
			UICtrl.instance.UpdateSpecialAmmo(specialAmmo);
			UICtrl.instance.UpdateLocked(isLocked);
		}
	}

	void Lock() {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Physics.Raycast(ray, out hit, 1000.0f);
			if (hit.collider != null) {
				Collider target = hit.collider;
				if (target.gameObject.tag.Equals("Enemy")) {
					Debug.Log(target.gameObject);
					lockedTarget = target.gameObject;
					isLocked = true;
				}
			}
		}
	}

	void Fire() {
		fireDelay -= Time.deltaTime;
		if (fireDelay < 0.0f) {
			if (Input.GetKey(KeyCode.LeftControl)) {
				if (ammo > 0) {
					--ammo;
					if (isLocked) {
						Instantiate(lockedBullet,
							transform.position,
							Quaternion.identity);
						isLocked = false;
					} else {
						Destroy(Instantiate(bullet,
							transform.position,
							Quaternion.identity), 2.0f);
					}

					fireDelay = 0.4f;
				}
			}
		}
	}

	void SpecialFire() {
		if (Input.GetKeyDown(KeyCode.Z)) {
			if (specialAmmo > 0) {
				--specialAmmo;
				Collider[] targets = Physics.OverlapSphere(transform.position, 30.0f);
				foreach(Collider target in targets) {
					if (target.tag == "Enemy") {
						if(target.gameObject.name != "Boss") {
							target.SendMessage("Hit");
							UICtrl.instance.AddScore(10);
						}
					}
				}
			}
		}
	}
}
