﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public float spawnTime = 5f;        // The amount of time between each spawn.
    public float spawnDelay = 3f;       // The amount of time before spawning starts.
    public GameObject[] enemies;        // Array of enemy prefabs.
	public float xMargin = 1f;		// Distance in the x axis the player can move before the camera follows.
	public float minX;
	public float maxX;
    public GameObject player;
	private float offsetX;
    void Start()
    {	
		player = GameObject.FindGameObjectWithTag("Player");
		offsetX = transform.position.x - player.transform.position.x;
        // Start calling the Spawn function repeatedly after a delay .
        InvokeRepeating("Spawn", spawnDelay, spawnTime);
    }

    void FixedUpdate()
    {
		if (player != null)
		{
        	TrackPlayer();
		}
    }

	bool CheckXMargin()
	{
		// Returns true if the distance between the camera and the player in the x axis is greater than the x margin.
		return Mathf.Abs(transform.position.x - player.transform.position.x) > xMargin;
	}

    void TrackPlayer()
    {
        // By default the target x and y coordinates of the camera are it's current x and y coordinates.
        float targetX = player.transform.position.x + offsetX;

        // The target x and y coordinates should not be larger than the maximum or smaller than the minimum.
        targetX = Mathf.Clamp(targetX, minX, maxX);

        // Set the camera's position to the target position with the same z component.
        transform.position = new Vector3(targetX, transform.position.y, transform.position.z);
    }

    void Spawn()
    {
        // Instantiate a random enemy.
        int enemyIndex = Random.Range(0, enemies.Length);
        Instantiate(enemies[enemyIndex], transform.position, transform.rotation);

        // Play the spawning effect from all of the particle systems.
        foreach (ParticleSystem p in GetComponentsInChildren<ParticleSystem>())
        {
            p.Play();
        }
    }
}
