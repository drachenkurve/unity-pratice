﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazziCtrl : MonoBehaviour
{
    public GameCtrl_03 gameCtrl;
    public Vector3 startPosition;
    [Range(1, 2)]
    public int playerNumber;
    public GameObject bombPrefab;
    public int layCount = 2;
    public float speed = 4f;
    KeyCode up, down, left, right, lay;
    Rigidbody2D itsRigidBody;
    Transform itsTransfrom;
    Animator itsAnimator;
    float layDelay = 1f;
    int power = 2;
    bool isAlive = true;
    void Start()
    {
        if (playerNumber == 1)
        {
            up = KeyCode.W;
            down = KeyCode.S;
            left = KeyCode.A;
            right = KeyCode.D;
            lay = KeyCode.Space;
        }
        else if (playerNumber == 2)
        {
            up = KeyCode.UpArrow;
            down = KeyCode.DownArrow;
            left = KeyCode.LeftArrow;
            right = KeyCode.RightArrow;
            lay = KeyCode.Return;
        }
        itsRigidBody = GetComponent<Rigidbody2D>();
        itsTransfrom = transform;
        itsAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        if (isAlive)
        {
            UpdateMove();
            UpdateLay();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("bazzi trigger");
        if (other.CompareTag("Boom"))
        {
            if (isAlive)
            {
                gameCtrl.PlayerDead(playerNumber);
            }
            itsAnimator.SetBool("IsDead", true);
            isAlive = false;
            ResetMove();
            Invoke("HideThis", 1.5f);
        }
        if (other.CompareTag("ItemPower"))
        {
            Destroy(other.gameObject);
            ++power;
        }
        if (other.CompareTag("ItemLay"))
        {
            Destroy(other.gameObject);
            ++layCount;
        }
        if (other.CompareTag("ItemSpeed"))
        {
            Destroy(other.gameObject);
            speed += 2.0f;
        }
    }

    void HideThis()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    void ResetMove()
    {
        itsAnimator.SetFloat("Speed", speed * 0.5f);
        itsAnimator.SetBool("WalkUp", false);
        itsAnimator.SetBool("WalkDown", false);
        itsAnimator.SetBool("WalkLeft", false);
        itsAnimator.SetBool("WalkRight", false);
        itsRigidBody.velocity = new Vector2(0.0f, 0.0f);

    }
    void UpdateMove()
    {
        ResetMove();
        itsAnimator.SetFloat("Speed", speed * 0.5f);
        itsAnimator.SetBool("WalkUp", false);
        itsAnimator.SetBool("WalkDown", false);
        itsAnimator.SetBool("WalkLeft", false);
        itsAnimator.SetBool("WalkRight", false);
        if (Input.GetKey(up))
        {
            itsRigidBody.velocity = new Vector2(0.0f, speed);
            itsAnimator.SetBool("WalkUp", true);
        }
        else if (Input.GetKey(down))
        {
            itsRigidBody.velocity = new Vector2(0.0f, -speed);
            itsAnimator.SetBool("WalkDown", true);
        }
        else if (Input.GetKey(left))
        {
            itsRigidBody.velocity = new Vector2(-speed, 0.0f);
            itsAnimator.SetBool("WalkLeft", true);
        }
        else if (Input.GetKey(right))
        {
            itsRigidBody.velocity = new Vector2(speed, 0.0f);
            itsAnimator.SetBool("WalkRight", true);
        }
        ClampMove();
    }

    void ClampMove()
    {
        Vector2 pos = itsTransfrom.position;
        pos.x = Mathf.Clamp(pos.x, -10f, 10f);
        pos.y = Mathf.Clamp(pos.y, -8f, 8f);
        itsTransfrom.position = pos;
    }

    void UpdateLay()
    {
        layDelay += Time.deltaTime;
        if (Input.GetKeyDown(lay))
        {
            if (layDelay >= 0.2f && layCount > 0)
            {
                layDelay = 0f;
                Lay();
            }
        }
    }

    void Lay()
    {
        --layCount;
        GameObject l = Instantiate(bombPrefab,
            new Vector3(Snap(itsTransfrom.position.x), Snap(itsTransfrom.position.y), 0.0f),
            Quaternion.identity);
        StartCoroutine(l.GetComponent<BombCtrl>().Boom(power));
        Invoke("AddLayCount", 2f);
    }

    float Snap(float coord)
    {
        return Mathf.RoundToInt(coord);
    }

    public void AddLayCount()
    {
        ++layCount;
    }

    public void ResetBazzi() {
        Debug.Log("reset bazzi");
        layCount = 2;
        power = 2;
        speed = 4f;
        isAlive = true;
        transform.position = startPosition;
        GetComponent<SpriteRenderer>().enabled = true;
        itsAnimator.SetBool("IsDead", false);
    }
}
