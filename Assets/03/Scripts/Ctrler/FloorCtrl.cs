﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCtrl : MonoBehaviour
{
    public GameObject floorPrefab;
    public void ResetThis()
    {
        Debug.Log("floor");
        for (int i = -10; i <= 10; ++i)
        {
			for (int j = -8; j <= 8; ++j)
			{
				Instantiate(floorPrefab, new Vector3(i, j, 0f),
					Quaternion.identity);
			}
        }
    }
}
