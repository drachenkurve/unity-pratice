﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageCtrl : MonoBehaviour
{
    public GameObject stagePrefab;
	GameObject prev;
    public void ResetThis()
    {
		if (prev != null)
		{
			Destroy(prev);
		}
		prev = Instantiate(stagePrefab, Vector3.zero, Quaternion.identity);
    }

}
