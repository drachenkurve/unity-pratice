﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCtrl : MonoBehaviour
{
    public LayerMask layerMask;
    public GameObject BoomCenterPrefab;
    public GameObject BoomMidPrefab;
    public GameObject BoomEndPrefab;
    public int power = 2;
    int tolerence = 1;
    GameCtrl_03 game;
    bool boomed = false;

    void Start()
    {
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Boom") && !boomed)
        {
            CancelInvoke();
            StartCoroutine(Boom(power));
        }
    }

    public IEnumerator Boom(int playerPower)
    {
        Debug.Log("Boom");
        yield return new WaitForSeconds(2f);
        power = playerPower;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        boomed = true;
        StartCoroutine(MakeBoom(Vector3.up, new Vector3(0f, 0f, 0f)));
        StartCoroutine(MakeBoom(Vector3.down, new Vector3(0f, 0f, 180f)));
        StartCoroutine(MakeBoom(Vector3.right, new Vector3(0f, 0f, 270f)));
        StartCoroutine(MakeBoom(Vector3.left, new Vector3(0f, 0f, 90f)));
        Destroy(Instantiate(BoomCenterPrefab, transform.position,
            Quaternion.identity), 0.5f);
    }

    IEnumerator MakeBoom(Vector3 dir, Vector3 rot)
    {
        Vector3 pos = transform.position;
        Vector2 pos2 = new Vector2(pos.x, pos.y);
        Vector2 dir2 = new Vector2(dir.x, dir.y);
        RaycastHit2D hit;

        for (int i = 1; i <= power; ++i)
        {
            hit = Physics2D.Raycast(pos2, dir2, i, layerMask);
            if (!hit.collider)
            {
                if (i == power)
                {
                    Destroy(Instantiate(BoomEndPrefab, pos + (i * dir),
                        Quaternion.Euler(rot)), 0.5f);
                }
                else
                {
                    Destroy(Instantiate(BoomMidPrefab, pos + (i * dir),
                        Quaternion.Euler(rot)), 0.5f);
                }
            }
            else if (hit.collider.CompareTag("Dtorable"))
            {
                Destroy(Instantiate(BoomEndPrefab, pos + (i * dir),
                    Quaternion.Euler(rot)), 0.5f);
                Destroy(hit.collider.gameObject);
                break;
            }
            else
            {
                break;
            }
        }
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
