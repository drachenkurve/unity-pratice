﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCtrl : MonoBehaviour
{
    public GameObject PowerItemPrefab;
    public GameObject LayItemPrefab;
    public GameObject SpeedItemPrefab;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Boom"))
        {
            Debug.Log("item?");
            switch (Random.Range(0, 10))
            {
                case 0:
					Instantiate(PowerItemPrefab, transform.position, Quaternion.identity);
                    break;
                case 1:
					Instantiate(LayItemPrefab, transform.position, Quaternion.identity);
                    break;
                case 2:
					Instantiate(SpeedItemPrefab, transform.position, Quaternion.identity);
                    break;
            }
            Destroy(gameObject);
        }
    }
}
