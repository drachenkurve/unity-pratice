﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTextCtrl : MonoBehaviour
{

    public Transform target;
    float offsetY = 1f;
    void Start()
    {

    }

    void LateUpdate()
    {
		if (target != null) {
        Vector2 pos = target.position;
        pos.y += offsetY;
        transform.position = pos;
		}
    }
}
